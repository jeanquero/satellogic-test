# REST Satellogic

# Welcome!

#Install step

1. Clone the project to your GitHub account
2. Use the following set of commands to build and test the project

```
# Setup
$ sudo docker build -t satellogic:1.0.0 .
$ cd docker
$ sudo docker-compose up
$ docker exec -it kafka_kafka1_1 kafka-topics --zookeeper zookeeper:2181 --create --topic my-topic-three --partitions 1 --replication-factor 3
> Created topic "topic-satellogic"
> Created topic "topic-satellogic-error"
```

#End Point 
```
# Post
http://[host]:8080/ping
```

#Payload
```
# Post
{
  "transaction_id": "1933",
    "payload": {
    	"message": "hello",
    	"force_error": false
    }
}
```


*Note: As it is a test I used Map to save the message but a KVS must be used