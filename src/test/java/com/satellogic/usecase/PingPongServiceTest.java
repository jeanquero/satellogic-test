package com.satellogic.usecase;


import com.satellogic.app.api.request.Payload;
import com.satellogic.app.api.request.PingPongApi;
import com.satellogic.app.api.response.PingPongResponse;
import com.satellogic.domain.inmemorysimple.InMemoryPingPongFailedRepository;
import com.satellogic.domain.inmemorysimple.InMemoryPingPongRepository;
import com.satellogic.domain.model.PingPongEntity;
import com.satellogic.infrastructure.exception.ForceErrorException;
import com.satellogic.infrastructure.exception.MaximumNumberAttemptsException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;

import static  org.mockito.Mockito.when;
import static  org.mockito.Mockito.any;

@RunWith(MockitoJUnitRunner.class)
@TestPropertySource(properties = {
        "satellagic.time.error=2"
})
public class PingPongServiceTest {

    @InjectMocks
    private PingPongService pingPongService;

    private InMemoryPingPongRepository repository = new InMemoryPingPongRepository();

    private InMemoryPingPongFailedRepository failedRepository = new InMemoryPingPongFailedRepository();

    @Mock
    private PingPongProducer pingService;


    @Before
    public void init() {
        ReflectionTestUtils.setField(pingPongService, "timeError", 2);
        failedRepository.save("1");
        failedRepository.save("1");
        failedRepository.save("1");
        repository.create("2", new  PingPongEntity("2","test"));
    }

    @Test(expected = ForceErrorException.class)
    public void sendForcerError() throws ForceErrorException, MaximumNumberAttemptsException {
        pingPongService.send(getPingPong("test",true));
    }

    @Test(expected = MaximumNumberAttemptsException.class)
    public void sendMaximumNumberAttempts() throws ForceErrorException, MaximumNumberAttemptsException {
        pingPongService.send(getPingPong("1",false));
    }

    @Test
    public void sendMessageExists() throws ForceErrorException, MaximumNumberAttemptsException {
        PingPongResponse response = pingPongService.send(getPingPong("2",false));
        Assert.assertEquals(response.getTransaction_id(), "2");
        Assert.assertEquals(response.getPayload().getMessage(), "test");
    }

    @Test
    public void send() throws ForceErrorException, MaximumNumberAttemptsException {
        PingPongResponse response = pingPongService.send(getPingPong("3",false));
        Assert.assertEquals(response.getTransaction_id(), "3");
        Assert.assertEquals(response.getPayload().getMessage(), "test");
    }



    private PingPongApi getPingPong(String id, Boolean force) {
        PingPongApi ping = new PingPongApi();
        ping.setTransaction_id(id);
        Payload pl = new Payload();
        pl.setMessage("test");
        pl.setForce_error(force);
        ping.setPayload(pl);
        return ping;
    }
}