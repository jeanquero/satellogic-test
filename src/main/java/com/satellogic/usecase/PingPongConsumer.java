package com.satellogic.usecase;

import com.satellogic.domain.inmemorysimple.InMemoryPingPongRepository;
import com.satellogic.domain.model.PingPongEntity;
import com.satellogic.usecase.port.PingPongConsumerUseCase;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PingPongConsumer implements PingPongConsumerUseCase {

    private InMemoryPingPongRepository repository = new InMemoryPingPongRepository();

    /**
     * this method is linsting the queue
     * @param ping
     */
    @Override
    @KafkaListener(topics ="${kafka.topic}", groupId = "${kafka.topic.group-id}",
            containerFactory = "pingPongKafkaListenerContainerFactory")
    public void Listen(PingPongEntity ping) {
        log.info(String.format("$$$$ => Consumed message: %s", ping));
        repository.create(ping.getTransaction_id(),ping);
        log.info(String.format("ping saved"));
    }
}
