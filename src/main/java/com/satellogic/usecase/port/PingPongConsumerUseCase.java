package com.satellogic.usecase.port;


import com.satellogic.domain.model.PingPongEntity;

public interface PingPongConsumerUseCase {
    void Listen(PingPongEntity message);
}
