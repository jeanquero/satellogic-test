package com.satellogic.usecase.port;

import com.satellogic.app.api.request.PingPongApi;

public interface PingPongProducerUseCase {
    void send(PingPongApi ping);
    void sendError(PingPongApi ping);
}
