package com.satellogic.usecase.port;

import com.satellogic.app.api.request.PingPongApi;
import com.satellogic.app.api.response.PingPongResponse;
import com.satellogic.infrastructure.exception.ForceErrorException;
import com.satellogic.infrastructure.exception.MaximumNumberAttemptsException;

public interface PingPongUseCase {

    PingPongResponse send(final PingPongApi ping) throws MaximumNumberAttemptsException, ForceErrorException;
}
