package com.satellogic.usecase;

import com.satellogic.app.api.request.PingPongApi;
import com.satellogic.domain.inmemorysimple.InMemoryPingPongFailedRepository;
import com.satellogic.domain.model.PingPongEntity;
import com.satellogic.usecase.port.PingPongProducerUseCase;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Log4j2
public class PingPongProducer implements PingPongProducerUseCase {

    private InMemoryPingPongFailedRepository repository = new InMemoryPingPongFailedRepository();

    private final KafkaTemplate<String, PingPongEntity> kafkaTemplate;
    @Value(value = "${kafka.topic}")
    private String TOPIC;

    @Value(value = "${kafka.topic-error}")
    private String TOPIC_ERROR;


    public PingPongProducer(KafkaTemplate<String, PingPongEntity> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     *  this method allows sending a message to the queue
     * @param ping
     */
    @Override
    public void send(PingPongApi ping) {
        log.info(String.format("$$$$ => send ping message: %s", ping.getPayload().getMessage()));
        ListenableFuture<SendResult<String, PingPongEntity>> future = this.kafkaTemplate.send(TOPIC,
               new  PingPongEntity(ping.getTransaction_id(),ping.getPayload().getMessage()));
        future.addCallback(new ListenableFutureCallback<SendResult<String, PingPongEntity>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.info("Unable to send message=[ {} ] due to : {}", ping.getPayload().getMessage(), ex.getMessage());
                repository.save(ping.getTransaction_id());
            }

            @Override
            public void onSuccess(SendResult<String, PingPongEntity> result) {
                log.info("Sent message=[ {} ] with offset=[ {} ]", ping.getPayload().getMessage(), result.getRecordMetadata().offset());
            }
        });
    }

    /**
     *  this method allows sending a message to the queue that receives the error
     * @param ping
     */
    @Override
    public void sendError(PingPongApi ping) {
        log.info(String.format("$$$$ => send error ping message: %s", ping.getPayload().getMessage()));
        ListenableFuture<SendResult<String, PingPongEntity>> future = this.kafkaTemplate.send(TOPIC_ERROR,
                new  PingPongEntity(ping.getTransaction_id(),ping.getPayload().getMessage()));
        future.addCallback(new ListenableFutureCallback<SendResult<String, PingPongEntity>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.info("Unable to send message=[ {} ] due to : {}", ping, ex.getMessage());
            }

            @Override
            public void onSuccess(SendResult<String, PingPongEntity> result) {
                log.info("Sent message=[ {} ] with offset=[ {} ]", ping, result.getRecordMetadata().offset());
            }
        });

    }
}
