package com.satellogic.usecase;

import com.satellogic.app.api.request.PingPongApi;
import com.satellogic.app.api.response.Payload;
import com.satellogic.app.api.response.PingPongResponse;
import com.satellogic.domain.inmemorysimple.InMemoryPingPongFailedRepository;
import com.satellogic.domain.inmemorysimple.InMemoryPingPongRepository;
import com.satellogic.domain.model.PingPongEntity;
import com.satellogic.infrastructure.exception.ForceErrorException;
import com.satellogic.infrastructure.exception.MaximumNumberAttemptsException;
import com.satellogic.usecase.port.PingPongUseCase;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Predicate;


@Service
@Log4j2
public class PingPongService implements PingPongUseCase {


    private InMemoryPingPongRepository repository = new InMemoryPingPongRepository();

    private InMemoryPingPongFailedRepository failedRepository = new InMemoryPingPongFailedRepository();


    @Autowired
    private PingPongProducer pingService;

    @Value(value = "${satellagic.time.error}")
    private Integer timeError;

    /**
     * This method allows validate and send message to the queue
     * @param ping
     * @return  @{@link PingPongResponse}
     * @throws MaximumNumberAttemptsException
     * @throws ForceErrorException
     */
    @Override
    public PingPongResponse send(final PingPongApi ping) throws MaximumNumberAttemptsException, ForceErrorException {
        log.info("Start send message");
        long startTime = System.nanoTime();
        // this is to check the fake error
        if(ping.getPayload().getForce_error()) {
            log.error(String.format("Error => force error PingPong ID: %s", ping.getTransaction_id()));
            failedRepository.save(ping.getTransaction_id());
            throw new ForceErrorException("Error -- Force Error Id = " + ping.getTransaction_id());
        }
        if (validatedTimeError().test(ping.getTransaction_id())) {
            log.error(String.format("Error => maximum number of attempts ID: %s", ping.getTransaction_id()));
            pingService.sendError(ping);
            throw new MaximumNumberAttemptsException(String.format("Error => maximum number of attempts ID: %s",
                    ping.getTransaction_id()));
        }

        Optional<PingPongEntity> resp = repository.findById(ping.getTransaction_id());

        if(resp.isPresent()) {
            log.info(String.format("$$$ => the message exists already", ping.getTransaction_id()));
            return PingPongResponse.builder()
                    .transaction_id(ping.getTransaction_id())
                    .payload(Payload.builder()
                            .message(resp.get().getMessage())
                            .processing_time(System.nanoTime() - startTime)
                            .build())
                    .build();
        }

        pingService.send(ping);
        log.info(String.format("$$$ => the message sent the queue", ping.getTransaction_id()));
        return PingPongResponse.builder()
                .transaction_id(ping.getTransaction_id())
                .payload(Payload.builder()
                        .message(ping.getPayload().getMessage())
                        .processing_time(System.nanoTime() - startTime)
                        .build())
                .build();
    }

    private Predicate<String> validatedTimeError() {
        return id -> {
          Optional<Integer> resp = failedRepository.findById(id);
          if(resp.isPresent())
              return resp.get() > timeError;
          else
              return false;
        };
    }









}
