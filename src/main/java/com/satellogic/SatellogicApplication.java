package com.satellogic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SatellogicApplication {

	public static void main(String[] args) {
		SpringApplication.run(SatellogicApplication.class, args);
	}

}
