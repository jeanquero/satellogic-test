package com.satellogic.infrastructure.exception;

public class MaximumNumberAttemptsException extends Exception {
    private static final long serialVersionUID = -5330068136795055851L;

    public MaximumNumberAttemptsException(String message) {
        super(message);
    }
}
