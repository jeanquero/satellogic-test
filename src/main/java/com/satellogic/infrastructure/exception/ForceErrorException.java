package com.satellogic.infrastructure.exception;

public class ForceErrorException extends Exception {
    private static final long serialVersionUID = -56436436795055851L;

    public ForceErrorException(String message) {
        super(message);
    }
}
