package com.satellogic.domain.inmemorysimple;



import com.satellogic.domain.model.PingPongEntity;

import java.util.*;

public class InMemoryPingPongRepository  {

    private final static Map<String, PingPongEntity> inMemoryDb = new HashMap<>();

    public PingPongEntity create(String id, PingPongEntity ping) {
        inMemoryDb.put(id, ping);
        return ping;
    }

    public Optional<PingPongEntity> findById(String id) {
        return Optional.ofNullable(inMemoryDb.get(id));
    }


}
