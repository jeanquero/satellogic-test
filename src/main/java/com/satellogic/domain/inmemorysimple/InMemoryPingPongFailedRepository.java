package com.satellogic.domain.inmemorysimple;



import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


public class InMemoryPingPongFailedRepository {

    private final static Map<String, Integer> inMemoryDb = new HashMap<>();

    public Integer save(String id) {
        inMemoryDb.put(id, inMemoryDb.getOrDefault(id,0) +1);
        return inMemoryDb.get(id);
    }

    public Optional<Integer> findById(String id) {
        return Optional.ofNullable(inMemoryDb.get(id));
    }


}
