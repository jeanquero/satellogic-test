package com.satellogic.domain.model;

public class PingPongEntity {

    private String transaction_id;
    private String message;

    public PingPongEntity() {
    }

    public PingPongEntity(String transaction_id, String message) {
        this.transaction_id = transaction_id;
        this.message = message;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
