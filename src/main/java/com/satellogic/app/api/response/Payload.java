package com.satellogic.app.api.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class Payload {

    private String message;
    private Long processing_time;
}
