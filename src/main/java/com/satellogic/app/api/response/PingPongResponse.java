package com.satellogic.app.api.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class PingPongResponse {

    private String transaction_id;
    private Payload payload;

}
