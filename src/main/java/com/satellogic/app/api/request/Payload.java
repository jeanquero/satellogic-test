package com.satellogic.app.api.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class Payload {

    @NotNull
    private String message;
    @NotNull
    private Boolean force_error;
}
