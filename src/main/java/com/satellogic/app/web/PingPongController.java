package com.satellogic.app.web;

import com.satellogic.app.api.request.PingPongApi;
import com.satellogic.app.api.response.PingPongResponse;
import com.satellogic.infrastructure.exception.ForceErrorException;
import com.satellogic.infrastructure.exception.MaximumNumberAttemptsException;
import com.satellogic.usecase.port.PingPongUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/ping")
public class PingPongController {

    @Autowired
    private PingPongUseCase servise;

    @PostMapping("")
    public PingPongResponse send(@Valid @RequestBody final PingPongApi ping) throws ForceErrorException,
            MaximumNumberAttemptsException {
        return servise.send(ping);
    }

}
